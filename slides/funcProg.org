#+TITLE:     Quick & Clean Programming
#+AUTHOR:    jean-luc.falcone@unige.ch
#+EMAIL:     jean-luc.falcone@unige.ch
#+DATE:      Octobre 2017
#+LANGUAGE:  fr
#+OPTIONS:   H:2 toc:nil
#+STARTUP: showall
#+startup: beamer
#+LATEX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_FRAME_LEVEL: 2
#+BEAMER_THEME: Frankfurt
#+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col) %8BEAMER_OPT(Opt)
#+latex_header: \usepackage{url}
#+LaTeX_HEADER: \usepackage{color}


* Programmation Fonctionelle

** Quoi ?

- Programmer avec des fonctions pures:
  - Peut remplacer un appel de fonction par son résultat
  - Pas d'effet de bord (observable)
  - Fonctions totales
  - Indépendantes
  - Composables

** Pourquoi ?

- Code modulaire
- Facile à tester
- Facile à débuguer
- Possibilité de raisonner

** Pourquoi pas ?

- Pas d'IO !
- Parfois plus lent
- Souvent pénible:
  - Si on ne fait l'effort d'apprendre à nouveau
  - Si on ne change pas de manière de penser
  - Si on utilise pas un langage fait pour...
 
* Exemple concret

** Génération de données aléatoires

- On aimerait écrire une librairie permettant de générer aléatoirement
  des données arbitraires.

*** Left							      :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.6
    :END:
#+begin_src scala
case class Date(
  year: Int,
  month: Int,
  day: Int
)
#+end_src

*** Right							      :BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :END:
#+begin_src scala
case class User(
  name: String,
  gender: Gender,
  birth: Date
)
#+end_src

*** Bas							    :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:

#+begin_src scala
sealed trait Gender
case object Male extends Gender
case object Female extends Gender
#+end_src

** Données à disposition

#+begin_src scala
val femaleNames = Vector( "Alice", "Bea", "Carol" )
val maleNames = Vector( "David", "Frank", "Guy" )
val surNames = Vector( "Jones", "Smith", "Williams" )

val daysPerMonth =
  Vector( 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 )
val daysPerMonthLeap = {
  daysPerMonth.updated( 1, 29 )
}
#+end_src

** Implémentation procédurale (1)

#+begin_src scala
def randomUser( rng: Random ): User = {
  val gender = if( rng.nextBoolean ) Female else Male
  val name = pickName( rng, gender )
  val birth = pickDate( rng )
  User( name, gender, birth)
}

def pickName( rng: Random, gender: Gender ): String = {
  val gender = if( rng.nextBoolean ) Female else Male
  val firstNames = 
    if( gender == Female ) femaleNames else maleNames
  val firstName = firstNames( rng.nextInt( firstNames.size ) )
  val surName = surNames( rng.nextInt( surNames.size ) )
  firstName + " " + surName
}
#+end_src

** Implémentation procédurale (2)

#+begin_src scala
def pickDate( rng: Random ): Date = {
  val year = rng.nextInt(80) + 1920
  val month = rng.nextInt(12) + 1
  val leapYear =
    if( year % 4 != 0 ) false
    else if( year % 100 != 0 ) true
    else if( year % 400 != 0 ) false
    else true
  val maxDay = {
    val n = month - 1
    if( leapYear ) daysPerMonthLeap(n) else daysPerMonth(n)
   }
  val day = rng.nextInt( maxDay ) + 1
  Date( year, month, day )
}
#+end_src

** RNG Fonctionel

#+begin_src scala
class PureRand private( private val state: Int ) {
  private def next = {
    val state2 = 1664525*state + 1013904223 
    if( state2 <0 ) new PureRand( -state2 )
    else new PureRand( state2 )
  }
  def nextInt( from: Int, to: Int ): (PureRand,Int) = {
    val n = (to-from)
    val i = state % n + from
    (next,i)
  }
  def nextBoolean: (PureRand,Boolean) = {
    val b = state % 2 == 1
    (next,b)
  }
}
#+end_src

** Implémentation fonctionnelle (1)

#+begin_src scala
def pickName( rng0: PureRand, gender: Gender ): 
  (PureRand,String) = {
  val (rng1,genB ) = rng0.nextBoolean
  val gender = if( genB ) Female else Male
  val firstNames = 
    if( gender == Female ) femaleNames else maleNames
  val (rng2,fnIdx) = rng1.nextInt( 0, firstNames.size )
  val firstName = firstNames( fnIdx )
  val (rng3,snIdx) = rng2.nextInt( 0, surNames.size )
  val surName = surNames( snIdx )
  val name = firstName + " " + surName
  (rng3,name)
}
#+end_src

* Abstraction

** Source Aléatoire

#+begin_src scala
trait Fake {

  type Rand[A]

  def int( min: Int, max: Int ): Rand[Int]
  def boolean(): Rand[Boolean]

}

def user( fake: Fake ): fake.Rand[User] = ???
#+end_src

** Année et mois

#+begin_src scala
def year( fake: Fake ) = fake.int( 1920, 2000 )
def month( fake: Fake ) = fake.int( 1, 13 )

def gender( fake: Fake ) = {
  val b = fake.nextBoolean //Rand[Boolean]
  ???
}
#+end_src

** Transformation

=Rand[A]= + =A=>B= $\to$ =Rand[B]=

\hspace{1em}

Par exemple:

- =Rand[Boolean]= + =Boolean=>Gender= $\to$ =Rand[Gender]=
- =Rand[Int]= + =Int=>String= $\to$ =Rand[String]=

** Transformation: =map=

#+begin_src scala
trait Fake {

  type Rand[A] <: RandLike[A]

  trait RandLike[A] {
    def map[B]( f: A=>B ): Rand[B]
  }

  def int( min: Int, max: Int ): Rand[Int]
  def boolean(): Rand[Boolean]
}
#+end_src

** Transformation: =map=

#+begin_src scala
trait Fake {

  type Rand[A] <: RandLike[A]

  trait RandLike[A] {
    def map[B]( f: A=>B ): Rand[B]
  }

  def int( min: Int, max: Int ): Rand[Int]
  def boolean(): Rand[Boolean]
  def pick[A]( as: Vector[A] ): Rand[A] =
    int( 0, as.size ).map( i => as(i) )
}
#+end_src

** Genre et nom de famille
#+begin_src scala
def year( fake: Fake ) = fake.int( 1920, 2000 )
def month( fake: Fake ) = fake.int( 1, 13 )

def gender( fake: Fake ) =
  fake.boolean.map( b => if(b) Female else Male )

def surname( fake: Fake ) = fake.pick( surNames )
#+end_src

** Prénom et jour de naissance

- Problème:
  - Le prénom dépend du genre et d'un entier
  - Le jour de naissance dépend du mois et de l'année

** Dépendences

\includegraphics[width=\textwidth]{deps.pdf}

** Transformation: =flatMap=

#+begin_src scala
trait Fake {

  type Rand[A] <: RandLike[A]

  trait RandLike[A] {
    def map[B]( f: A=>B ): Rand[B]
    def flatMap[B]( f: A=>Rand[B] ): Rand[B]
  }
}
#+end_src

** Preuve: =flatMap=

#+begin_src scala
def combine2[A,B,C]( 
  ra: Rand[A], rb: Rand[B], f: (A,B)=>C 
): Rand[C] = 
  ra.flatMap{ a => rb.map{ b => f(a,b) } }
#+end_src


** Prénom et nom

#+begin_src scala
def surname( fake: Fake2 ) = fake.pick( surNames )

def firstname( g: Gender, fake: Fake2 ) =
  if( g == Female ) fake.pick( femaleNames )
    else fake.pick( maleNames )

def name( fake: Fake2, g: Gender ) =
  firstname( g, fake ).flatMap{ fn =>
    surname(fake).map( sn => fn + " " + sn )
  }
#+end_src

** Su-sucre syntaxique (1)

#+begin_src scala
def name( g: Gender, fake: Fake ) =
  for {
    fn <- firstname(g, fake )
    sn <- surname( fake )
  } yield fn + " " + sn
#+end_src


** Su-sucre syntaxique (2)

#+begin_src scala
case class RandomUser[F<:Fake2]( fake: F ) {
  /* ... */

  def name( g: Gender ) =
    for {
      fn <- firstname(g)
      sn <- surname
    } yield fn + " " + sn
  }
}
#+end_src


** Résultat "Final"

#+begin_src scala
case class RandomUser[F<:Fake2]( fake: F ) {
  /* ... */
  def user = for {
    g <- gender
    n <- name(g)
    d <- date
  } yield User( n, g, d )
}
#+end_src

* Concrétisation

** Générateur constant

#+begin_src scala
object Constant extends Fake {

  type Rand[A] = Value[A]

  case class Value[A]( get: A ) extends RandLike[A] {
    def map[B]( f: A=>B ): Rand[B] = Value( f(get) )
    def flatMap[B]( f: A=>Rand[B] ): Rand[B] = f(get)
  }

  def int( min: Int, max: Int ): Rand[Int] = 
    Value( (min+max)/2 )
  def boolean(): Rand[Boolean] = Value( true )

}
#+end_src

** Générateur constant: Utilisation

#+begin_src scala
val ruc = RandomUser( Constant )

val u1 = ruc.user.get

//=> User(Bea Smith,Female,Date(1960,7,16))
#+end_src

** Générateur impur

#+begin_src scala
object Random extends Fake {
  import scala.util.Random
  type Rand[A] = Compute[A]

  case class Compute[A]( run: Random=>A ) extends RandLike[A] {
    def map[B]( f: A=>B ): Rand[B] =
      Compute( rng => f( run(rng) ) )
    def flatMap[B]( f: A=>Rand[B] ): Rand[B] =
      Compute( rng => f( run(rng) ).run(rng) )
  }
  def int( min: Int, max: Int ): Rand[Int] =
    Compute( rng => rng.nextInt(max-min) + min )
  def boolean(): Rand[Boolean] =
    Compute( rng => rng.nextBoolean )
}
#+end_src

** Générateur impur: Utilisation

#+begin_src scala
val rur = RandomUser( Random )
val rand = new scala.util.Random(123)
val u2 = rur.user.run( rand )
println( u2 )

//=> User(Carol Williams,Female,Date(1989,4,18))
#+end_src

** Générateur pur

#+begin_src scala
object Pure extends Fake {
  type Rand[A] = PR[A]
  case class PR[A]( run: (PureRand)=>(PureRand,A) ) 
   extends RandLike[A] {
    def map[B]( f: A=>B ): Rand[B] =
      PR{ rng0 =>
        val (rng1,a) = run(rng0 )
        (rng1,f(a))
      }
    def flatMap[B]( f: A=>Rand[B] ): Rand[B] = PR{ rng0 =>
      val (rng1,a) = run(rng0)
      f(a).run(rng1)
    }
  }
  /*...*/
#+end_src

** Générateur pur: Utilisation

#+begin_src scala
val rup = RandomUser( Pure )
val prand = onePure.PureRand.init(321)
val (prand2,u3) = rup.user.run( prand )
println( u3 )

//=> User(David Smith,Male,Date(1927,11,8))
#+end_src

** Générateur exhaustif

#+begin_src scala
object Exhaustive extends Fake {
  type Rand[A] = Exhaust[A]

  case class Exhaust[A]( as: Stream[A] ) extends RandLike[A] {
    def map[B]( f: A=>B ): Rand[B] =
      Exhaust( as.map(f) )
    def flatMap[B]( f: A=>Rand[B] ): Rand[B] = {
      val g = (a:A) => f(a).as
      Exhaust( as.flatMap(g))
    }
  }

  def int( min: Int, max: Int ): Rand[Int] =
    Exhaust( (min until max).toStream )
  def boolean(): Rand[Boolean] =
    Exhaust( Stream( true, false ) )
}
#+end_src

** Générateur exhaustif: Utilisation

#+begin_src scala
val rus = RandomUser( Exhaustive )
rus.user.stream.foreach( println )
#+end_src

** Merci pour votre attention

Code et slides:

 \LARGE
http://bit.ly/2yPAdl1

