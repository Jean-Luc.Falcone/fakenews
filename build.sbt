organization := "ch.unige"
name := "fakeNews"
version := "0.0.1"
scalaVersion := "2.12.3"

libraryDependencies ++= Seq()

scalacOptions ++= Seq( "-deprecation", "-feature", "-language:higherKinds")

scalaSource in Compile := baseDirectory(_ / "src").value

scalaSource in Test := baseDirectory(_ / "test").value

resourceDirectory in Compile := baseDirectory.value / "resources"

