package ch.unige.fake
package four

trait Fake2 {

  type Rand[A] <: RandLike[A]

  trait RandLike[A] {
    def map[B]( f: A=>B ): Rand[B]
    def flatMap[B]( f: A=>Rand[B] ): Rand[B]
  }

  def int( min: Int, max: Int ): Rand[Int]
  def boolean(): Rand[Boolean]

  def pick[A]( as: Vector[A] ): Rand[A] =
    int( 0, as.size ).map( i => as(i) )

  def list[A]( ra: Rand[A], n: Int ): Rand[List[A]] = {
    var res = ra.map( _ => List.empty[A] )
    var i = n 
    while( i > 0 ) {
      res = res.flatMap( as => ra.map( a => a::as ) )
      i -= 1
    }
    res
  }
}

case class RandomUser[F<:Fake2]( fake: F ) {
  val femaleNames = Vector( "Alice", "Bea", "Carol" )
  val maleNames = Vector( "David", "Frank", "Guy" )
  val surNames = Vector( "Jones", "Smith", "Williams" )

  def gender =
    fake.boolean.map( b => if(b) Female else Male )
  def year =
    fake.int( 1920, 2000 )
  def month =
    fake.int( 1, 13 )

  def firstname( g: Gender) =
    if( g == Female ) fake.pick( femaleNames )
    else fake.pick( maleNames )
  def surname  = fake.pick( surNames )

  val daysPerMonth =
    Vector( 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 )
  val daysPerMonthLeap = {
    daysPerMonth.updated( 1, 29 )
  }
  def day( year: Int, month: Int ) = {
    val leapYear =
      if( year % 4 != 0 ) false
      else if( year % 100 != 0 ) true
      else if( year % 400 != 0 ) false
      else true
    val maxDay = {
      val n = month - 1
      if( leapYear ) daysPerMonthLeap(n) else daysPerMonth(n)
    }
    fake.int( 1, maxDay+1 )
  }


  def name( g: Gender ) =
    for {
      fn <- firstname(g)
      sn <- surname
    } yield fn + " " + sn

  def date = for {
    y <- year
    m <- month
    d <- day(y,m)
  } yield Date( y, m, d )

  def user = for {
    g <- gender
    n <- name(g)
    d <- date
  } yield User( n, g, d )


}
