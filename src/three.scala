package ch.unige.fake
package three

trait Fake2 {

  type Rand[A] <: RandLike[A]

  trait RandLike[A] {
    def map[B]( f: A=>B ): Rand[B]
    def flatMap[B]( f: A=>Rand[B] ): Rand[B]
  }

  def int( min: Int, max: Int ): Rand[Int]
  def boolean(): Rand[Boolean]

  def pick[A]( as: Vector[A] ): Rand[A] =
    int( 0, as.size ).map( i => as(i) )
}

object RandomUser3 {
  val femaleNames = Vector( "Alice", "Bea", "Carol" )
  val maleNames = Vector( "David", "Frank", "Guy" )
  val surNames = Vector( "Jones", "Smith", "Williams" )

  def gender( fake: Fake2 ) =
    fake.boolean.map( b => if(b) Female else Male )
  def year( fake: Fake2 ) =
    fake.int( 1920, 2000 )
  def month( fake: Fake2 ) =
    fake.int( 1, 13 )

  def firstname( g: Gender, fake: Fake2 ) =
    if( g == Female ) fake.pick( femaleNames )
    else fake.pick( maleNames )
  def surname( fake: Fake2 ) = fake.pick( surNames )
  def name( fake: Fake2 )( g: Gender ) =
    firstname( g, fake ).flatMap{ fn =>
      surname(fake).map( sn => fn + " " + sn )
    }

  val daysPerMonth =
    Vector( 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 )
  val daysPerMonthLeap = {
    daysPerMonth.updated( 1, 29 )
  }
  def day( year: Int, month: Int, fake: Fake2 ) = {
    val leapYear =
      if( year % 4 != 0 ) false
      else if( year % 100 != 0 ) true
      else if( year % 400 != 0 ) false
      else true
    val maxDay = {
      val n = month - 1
      if( leapYear ) daysPerMonthLeap(n) else daysPerMonth(n)
    }
    fake.int( 1, maxDay+1 )
  }
  def date( fake: Fake2 ) = {
    year(fake).flatMap{ y =>
      month(fake).flatMap{ m =>
        day( y, m, fake ).map{ d =>
          Date( y, m, d )
        }
      }
    }
  }
  def user( fake: Fake2 ) = {
    gender(fake).flatMap { g =>
      name(fake)(g).flatMap { n =>
        date(fake).map{ d =>
          User( n, g, d )
        }
      }
    }
  }

}
