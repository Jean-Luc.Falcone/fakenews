package ch.unige.fake

case class Date(
  year: Int,
  month: Int,
  day: Int
)

sealed trait Gender
case object Male extends Gender
case object Female extends Gender

case class User(
  name: String,
  gender: Gender,
  birth: Date
)

