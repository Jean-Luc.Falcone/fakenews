package ch.unige.fake
package one

import scala.util.Random

object RandomUser1 {

  val femaleNames = Vector( "Alice", "Bea", "Carol" )
  val maleNames = Vector( "David", "Frank", "Guy" )
  val surNames = Vector( "Jones", "Smith", "Williams" )

  val daysPerMonth =
    Vector( 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 )
  val daysPerMonthLeap = {
    daysPerMonth.updated( 1, 29 )
  }


  def randomUser( rng: Random ): User = {
    val gender = if( rng.nextBoolean ) Female else Male
    val firstNames = if( gender == Female ) femaleNames else maleNames
    val firstName = firstNames( rng.nextInt( firstNames.size ) )
    val surName = surNames( rng.nextInt( surNames.size ) )
    val name = firstName + " " + surName
    val year = rng.nextInt(80) + 1920
    val month = rng.nextInt(12) + 1
    val leapYear =
      if( year % 4 != 0 ) false
      else if( year % 100 != 0 ) true
      else if( year % 400 != 0 ) false
      else true
    val maxDay = {
      val n = month - 1
      if( leapYear ) daysPerMonthLeap(n) else daysPerMonth(n)
    }
    val day = rng.nextInt( maxDay ) + 1
    val birth = Date( year, month, day )
    User( name, gender, birth)
  }

} 

object RandomUser2 {

  val femaleNames = Vector( "Alice", "Bea", "Carol" )
  val maleNames = Vector( "David", "Frank", "Guy" )
  val surNames = Vector( "Jones", "Smith", "Williams" )

  val daysPerMonth =
    Vector( 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 )
  val daysPerMonthLeap = {
    daysPerMonth.updated( 1, 29 )
  }

  def pickName( rng: Random, gender: Gender ): String = {
    val gender = if( rng.nextBoolean ) Female else Male
    val firstNames = if( gender == Female ) femaleNames else maleNames
    val firstName = firstNames( rng.nextInt( firstNames.size ) )
    val surName = surNames( rng.nextInt( surNames.size ) )
    firstName + " " + surName
  }

  
  def pickDate( rng: Random ): Date = {
    val year = rng.nextInt(80) + 1920
    val month = rng.nextInt(12) + 1
    val leapYear =
      if( year % 4 != 0 ) false
      else if( year % 100 != 0 ) true
      else if( year % 400 != 0 ) false
      else true
    val maxDay = {
      val n = month - 1
      if( leapYear ) daysPerMonthLeap(n) else daysPerMonth(n)
    }
    val day = rng.nextInt( maxDay ) + 1
    Date( year, month, day )
  }


  def randomUser( rng: Random ): User = {
    val gender = if( rng.nextBoolean ) Female else Male
    val name = pickName( rng, gender )
    val birth = pickDate( rng )
    User( name, gender, birth)
  }

} 
