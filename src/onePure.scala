package ch.unige.fake
package onePure

class PureRand private( private val state: Int ) {
  private def next = {
    val state2 = 1664525*state + 1013904223 
    if( state2 <0 ) new PureRand( -state2 )
    else new PureRand( state2 )
  }
  def nextInt( from: Int, to: Int ): (PureRand,Int) = {
    val n = (to-from)
    val i = state % n + from
    (next,i)
  }
  def nextBoolean: (PureRand,Boolean) = {
    val b = state % 2 == 1
    (next,b)
  }
  override def toString = s"PureRand($state)"
}
object PureRand {
  def init( seed: Int ) = ( new PureRand(seed) ).next
}

object RandomName {
  val femaleNames = Vector( "Alice", "Bea", "Carol" )
  val maleNames = Vector( "David", "Frank", "Guy" )
  val surNames = Vector( "Jones", "Smith", "Williams" )

  def pickName( rng0: PureRand, gender: Gender ): (PureRand,String) = {
    val (rng1,genB ) = rng0.nextBoolean
    val gender = if( genB ) Female else Male
    val firstNames = if( gender == Female ) femaleNames else maleNames
    val (rng2,fnIdx) = rng1.nextInt( 0, firstNames.size )
    val firstName = firstNames( fnIdx )
    val (rng3,snIdx) = rng2.nextInt( 0, surNames.size )
    val surName = surNames( snIdx )
    val name = firstName + " " + surName
    (rng3,name)
  }

}
