package ch.unige.fake
package five

import four.{Fake2=>Fake}

object Constant extends Fake {

  type Rand[A] = Value[A]

  case class Value[A]( get: A ) extends RandLike[A] {
    def map[B]( f: A=>B ): Rand[B] = Value( f(get) )
    def flatMap[B]( f: A=>Rand[B] ): Rand[B] = f(get)
  }

  def int( min: Int, max: Int ): Rand[Int] = Value( (min+max)/2 )
  def boolean(): Rand[Boolean] = Value( true )

}

object Random extends Fake {

  import scala.util.Random
  type Rand[A] = Compute[A]

  case class Compute[A]( run: Random=>A ) extends RandLike[A] {
    def map[B]( f: A=>B ): Rand[B] =
      Compute( rng => f( run(rng) ) )
    def flatMap[B]( f: A=>Rand[B] ): Rand[B] =
      Compute( rng => f( run(rng) ).run(rng) )
    def runList( rng: Random, n: Int ): List[A] = {
      var i = 0
      var as = List.empty[A]
      while( i <= n ) {
        val a = run( rng )
        as ::= a
        i += 1
      }
      as
    }

  }

  def int( min: Int, max: Int ): Rand[Int] =
    Compute( rng => rng.nextInt(max-min) + min )
  def boolean(): Rand[Boolean] =
    Compute( rng => rng.nextBoolean )


}

object Pure extends Fake {
  import onePure.PureRand
  type Rand[A] = PR[A]
  case class PR[A]( run: (PureRand)=>(PureRand,A) ) extends RandLike[A] {
    def map[B]( f: A=>B ): Rand[B] =
      PR{ rng0 =>
        val (rng1,a) = run(rng0 )
        (rng1,f(a))
      }
    def flatMap[B]( f: A=>Rand[B] ): Rand[B] = PR{ rng0 =>
      val (rng1,a) = run(rng0)
      f(a).run(rng1)
    }
    def runList( rng0: PureRand, n: Int ): (PureRand,List[A]) = {
      var state = rng0
      var as = List.empty[A]
      var i = 0
      while( i <= n ) {
        val (s,a) = run(state)
        as ::= a
        state = s
        i += 1
      }
      (state,as)
      }

  }

  def int( min: Int, max: Int ): Rand[Int] =
    PR( rng => rng.nextInt(min,max) )
  def boolean(): Rand[Boolean] =
    PR( rng => rng.nextBoolean )

}

object Exhaustive extends Fake {
  type Rand[A] = Exhaust[A]

  case class Exhaust[A]( stream: Stream[A] ) extends RandLike[A] {
    def map[B]( f: A=>B ): Rand[B] =
      Exhaust( stream.map(f) )
    def flatMap[B]( f: A=>Rand[B] ): Rand[B] = {
      val g = (a:A) => f(a).stream
      Exhaust( stream.flatMap(g))
    }
  }

  def int( min: Int, max: Int ): Rand[Int] =
    Exhaust( (min until max).toStream )
  def boolean(): Rand[Boolean] =
    Exhaust( Stream( true, false ) )

}
object FiveDemo extends App {

  import ch.unige.fake.four.RandomUser
  import ch.unige.fake.onePure.PureRand

  println( "== CONSTANT ===============" )
  val ruc = RandomUser( Constant )
  val u1 = ruc.user.get
  println( u1 )

  println( "== IMPUR ===============" )
  val rur = RandomUser( Random )
  val rand = new scala.util.Random(123)
  val u2 = rur.user.run( rand )
  println( u2 )

  println( "== PUR ===============" )
  val rup = RandomUser( Pure )
  val prand = onePure.PureRand.init(321)
  val (prand2,u3) = rup.user.run( prand )
  println( u3 )

  println( "== EXHAUSTIF ===============" )
  val rus = RandomUser( Exhaustive )
  rus.user.stream.foreach( println )
}


