package ch.unige.fake
package two

trait Fake0 {

  type Rand[A]

  def int( min: Int, max: Int ): Rand[Int]
  def boolean(): Rand[Boolean]

}

trait Fake1 {

  type Rand[A] <: RandLike[A]

  trait RandLike[A] {
    def map[B]( f: A=>B ): Rand[B]
  }

  def int( min: Int, max: Int ): Rand[Int]
  def boolean(): Rand[Boolean]

  def pick[A]( as: Vector[A] ): Rand[A] =
    int( 0, as.size ).map( i => as(i) )
}

object RandomUser3 {
  val surNames = Vector( "Jones", "Smith", "Williams" )

  def gender( fake: Fake1 ) =
    fake.boolean.map( b => if(b) Female else Male )
  def year( fake: Fake1 ) =
    fake.int( 1920, 2000 )
  def month( fake: Fake1 ) =
    fake.int( 1, 13 )
  def surname( fake: Fake1 ) = fake.pick( surNames )
}


